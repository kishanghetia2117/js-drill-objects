// Returns a copy of the object where the keys have become the values and the values the keys.
// Assume that all of the object's values will be unique and string serializable.
// http://underscorejs.org/#invert

function invert(obj) {

    const invertedObject = {};

    for (let key in obj) {
        invertedObject[obj[key]] = key;
    }
    return invertedObject;
}

module.exports = invert;