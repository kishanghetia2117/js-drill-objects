// Like map for arrays, but for objects. Transform the value of each property in turn by passing it to the callback function.
// http://underscorejs.org/#mapObject

function mapObject(object, cb) {
    const Transform = {}

    for (let property in object) {
        Transform[property] = cb(object[property]);
    }
    return Transform;
}

module.exports = mapObject;