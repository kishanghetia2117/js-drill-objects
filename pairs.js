// Convert an object into a list of [key, value] pairs.
// http://underscorejs.org/#pairs

function pairs(object) {
    return Object.keys(object).map(key => [key, object[key]]);
}

module.exports = pairs;