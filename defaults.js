// Fill in undefined properties that match properties on the `defaultProps` parameter object.
// Return `obj`.
// http://underscorejs.org/#defaults

const defaults = (obj, defaultProps) => {
    const properties = Object.keys(defaultProps);

    properties.forEach((prop) => {
        if (obj[prop] === undefined) {
            obj[prop] = defaultProps[prop];
        }
    });
    return obj;
};

module.exports = defaults;