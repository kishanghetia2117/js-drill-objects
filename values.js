// Return all of the values of the object's own properties.
// Ignore functions
// http://underscorejs.org/#values

function values(obj) {
    const objValues = [];

    for (let prop in obj) {
        if (typeof obj[prop] !== "function") {
            objValues.push(obj[prop])
        }
    }
    return objValues;
}

module.exports = values;