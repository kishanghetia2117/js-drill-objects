const defaults = require("../defaults.js");

const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };
const defaultProps = { hiddenIdentity: 'Batman', heroClass: 'cape fighter', location: 'Gotham' };

console.log(defaults(testObject, defaultProps));