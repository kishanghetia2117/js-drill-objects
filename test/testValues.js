const values = require("../values.js");

const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };

const objectWithFunction = {
    firstName: "kishan",
    lastName: "ghetia",
    id: 007,
    fullName: function () {
        console.log("this is a function")
    }
};

objArray = values(testObject);

if (objArray.length > 0) {
    console.log(objArray);
} else {
    console.log("object is empty")
}

objArray = values(objectWithFunction);

if (objArray.length > 0) {
    console.log(objArray);
} else {
    console.log("object is empty")
}