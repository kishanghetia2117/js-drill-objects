const invert = require("../invert.js");

const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };

invertObj = invert(testObject);

if (Object.keys(invertObj).length > 0) {
    console.log(invertObj);
} else {
    console.log("object is empty")
}
